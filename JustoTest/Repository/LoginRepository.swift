//
//  LoginRepository.swift
//  JustoTest
//
//  Created by Jorge Luis Calvo on 02/12/20.
//

import Foundation
import UIKit

class LoginRepository {

    typealias JSONDictionary = [String: Any]

    private var defaultSession = URLSession(configuration: .default)
    private var dataTask: URLSessionDataTask?

    private var apiAddress = "https://randomuser.me/api/"

    func getUserInformation(completion: @escaping  (LoginResponse) ->  Void)  {

        dataTask?.cancel()
        if let urlComponents = URLComponents(string: apiAddress) {

            guard let url = urlComponents.url else {
                return
            }

            dataTask = defaultSession.dataTask(with: url) { [ weak self] data, response, error in

                defer {
                    self?.dataTask = nil
                }

                guard let data = data, let response = response as? HTTPURLResponse,
                      response.statusCode == 200  else {
                    return
                }
                //let text = try? JSONSerialization.jsonObject(with: data, options: []) as? JSONDictionary

                print(response.statusCode)

                let decoder = JSONDecoder()
                do {
                    let decodedResponse = try decoder.decode(LoginResponse.self, from: data)
                    completion(decodedResponse)
                } catch {
                    print(error)
                    return
                }


            }

            dataTask?.resume()

        }

    }

    func getUserImage(url: URL, completion: @escaping (UIImage?) -> Void) {
        dataTask?.cancel()

        dataTask = defaultSession.dataTask(with: url) { [ weak self] data, response, error in

            defer {
                self?.dataTask = nil
            }

            guard let data = data, let response = response as? HTTPURLResponse,
                  response.statusCode == 200  else {
                return
            }
                let image = UIImage(data: data)
                completion(image)
        }
        dataTask?.resume()
    }
}
