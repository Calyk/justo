//
//  UserViewController.swift
//  JustoTest
//
//  Created by Jorge Luis Calvo on 02/12/20.
//

import Foundation
import UIKit

protocol UserViewDelegate: UIViewController {
    func fillUI(user: User?)
    func fillUserPicture(image: UIImage) 
}

class UserViewController: UIViewController {

    private let lbName: UILabel = {
        let label = UILabel(frame: .zero)
        label.textColor = UIColor(named: "TitleBlack")
        label.font = UIFont.boldSystemFont(ofSize: 24)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let lbAdress: UILabel = {
        let label = UILabel(frame: .zero)
        label.textColor = UIColor(named: "TitlePink")
        label.font = UIFont.systemFont(ofSize: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let lbCountry: UILabel = {
        let label = UILabel(frame: .zero)
        label.textColor = UIColor(named: "TitleRed")
        label.font = UIFont.systemFont(ofSize: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let ivUser: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.contentMode = .scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()

    private let btLocation: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = UIColor(named: "TitleBlue")
        button.setTitle("Ubicación", for: .normal)
        return button
    }()

    private let viewModel: UserViewModel = {
        let viewModel = UserViewModel()
        return viewModel
    }()

    required init?(coder: NSCoder) {
        fatalError("Not using coder")
    }

    required init() {
        super.init(nibName: nil, bundle: nil)
        viewModel.viewDelegate = self
    }

    override func viewDidLoad() {
        setupUI()
        setupConstrains()
        setupTargets()
        viewModel.callLoginRequest()
    }

    func setupUI(){
        view.addSubview(lbName)
        view.addSubview(lbAdress)
        view.addSubview(lbCountry)
        view.addSubview(ivUser)
        view.addSubview(btLocation)
        view.backgroundColor = UIColor(named: "BackgroundGreen")
        btLocation.layer.cornerRadius = 10
    }

    func setupConstrains() {
        NSLayoutConstraint.activate([
            lbName.topAnchor.constraint(equalTo: view.topAnchor, constant: 32),
            lbName.heightAnchor.constraint(equalToConstant: 30),
            lbName.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 32),

            lbAdress.topAnchor.constraint(equalTo: lbName.bottomAnchor, constant: 16),
            lbAdress.heightAnchor.constraint(equalToConstant: 30),
            lbAdress.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 32),

            lbCountry.topAnchor.constraint(equalTo: lbAdress.bottomAnchor,constant: 16),
            lbCountry.heightAnchor.constraint(equalToConstant: 30),
            lbCountry.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 32),

            ivUser.topAnchor.constraint(equalTo: lbAdress.bottomAnchor,constant: 32),
            ivUser.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 32),
            ivUser.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -32),
            ivUser.bottomAnchor.constraint(equalTo: btLocation.topAnchor, constant: -16),

            btLocation.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            btLocation.widthAnchor.constraint(equalToConstant: 100),
            btLocation.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -32),
        ])
    }

    func setupTargets() {
        btLocation.addTarget(self, action: #selector(openLocation), for: .touchUpInside)
    }

    @objc func openLocation() {
        viewModel.openMap()
    }
}

extension UserViewController: UserViewDelegate {
    func fillUI(user: User?) {
        guard let user = user else { return }
        DispatchQueue.main.async {
            self.lbName.text = "\(user.name.first) \(user.name.last) "
            self.lbAdress.text = "\(user.location.street.name) \(user.location.street.number)"
            self.lbCountry.text = "\(user.location.country), \(user.location.postcode)"
        }
        viewModel.getImages()
    }

    func fillUserPicture(image: UIImage) {
        DispatchQueue.main.async {
            self.ivUser.image = image
        }
    }

}
