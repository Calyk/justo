//
//  User.swift
//  JustoTest
//
//  Created by Jorge Luis Calvo on 02/12/20.
//

import Foundation
import UIKit

struct User: Decodable {
    var gender: String
    var name: Name
    var location: Address
    var email: String
    var phone: String
    var cell: String
    var picture: Picture
}

struct Name: Decodable {
    var title: String
    var first: String
    var last: String
}

struct Picture: Decodable {
    var large: String
    var medium: String
    var thumbnail: String
}
