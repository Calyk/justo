//
//  LoginResponse.swift
//  JustoTest
//
//  Created by Jorge Luis Calvo on 02/12/20.
//

import Foundation

struct LoginResponse: Decodable {
    var results: [User]
}
