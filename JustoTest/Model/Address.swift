//
//  Address.swift
//  JustoTest
//
//  Created by Jorge Luis Calvo on 02/12/20.
//

import Foundation

struct Address: Decodable {
    var street: Street
    var city: String
    var state: String
    var postcode: String
    var country: String
    var coordinates: Coordinates

    enum CodingKeys: String, CodingKey {
        case street
        case city
        case state
        case postcode
        case country
        case coordinates
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        do {
            street = try container.decode(Street.self, forKey: .street)
            city = try container.decode(String.self, forKey: .city)
            state = try container.decode(String.self, forKey: .state)
            country = try container.decode(String.self, forKey: .country)
            coordinates = try container.decode(Coordinates.self, forKey: .coordinates)
            postcode = try container.decode(String.self, forKey: .postcode)
        } catch {
            street = try container.decode(Street.self, forKey: .street)
            city = try container.decode(String.self, forKey: .city)
            state = try container.decode(String.self, forKey: .state)
            country = try container.decode(String.self, forKey: .country)
            coordinates = try container.decode(Coordinates.self, forKey: .coordinates)
            let postCodeNumber = try container.decode(Int.self, forKey: .postcode)
            postcode = String(postCodeNumber)
            print("Int")
        }
    }
}

struct Street: Decodable {
    var number: Int
    var name: String
}

struct Coordinates: Decodable {
    var latitude: String
    var longitude: String
}
