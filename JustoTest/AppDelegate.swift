//
//  AppDelegate.swift
//  JustoTest
//
//  Created by Jorge Luis Calvo on 02/12/20.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        let rootController = UserViewController()
        let navController = UINavigationController(rootViewController: rootController)
        navController.navigationBar.isHidden = true
        window?.rootViewController = navController
        
        return true
    }

}

