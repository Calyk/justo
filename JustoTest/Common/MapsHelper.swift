    //
    //  MapsHelper.swift
    //  JustoTest
    //
    //  Created by Jorge Luis Calvo on 07/12/20.
    //

    import Foundation
    import MapKit

    protocol MapsHelper {
        func openMap(coordinates: Coordinates)
    }

    class MapsHelperImp: MapsHelper {

        func openMap(coordinates: Coordinates) {
            guard let latitude:CLLocationDegrees = Double(coordinates.latitude) else { return }
            guard let longitude: CLLocationDegrees = Double(coordinates.longitude) else { return }

            let regionDistance:CLLocationDistance = 10000
            let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
            let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
            let options = [
                MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
            ]
            let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            mapItem.name = "Tu Ubicación"
            mapItem.openInMaps(launchOptions: options)
        }
    }
