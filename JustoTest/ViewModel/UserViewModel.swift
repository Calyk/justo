//
//  UserViewModel.swift
//  JustoTest
//
//  Created by Jorge Luis Calvo on 02/12/20.
//

import Foundation

class UserViewModel {

    var repository = LoginRepository()
    var user: User?
    let mapHelper = MapsHelperImp()
    weak var viewDelegate: UserViewDelegate?

    func callLoginRequest() {
        repository.getUserInformation { [weak self] response in
            self?.user = response.results.first
            self?.updateUI()
        }
    }

    func updateUI() {
        viewDelegate?.fillUI(user: user)
    }

    func getImages() {
        guard let user = user else { return}
        guard let imageURL = URL(string: user.picture.large) else { return}
        repository.getUserImage(url: imageURL) { [weak self] image in
            guard let image = image else { return}
            self?.viewDelegate?.fillUserPicture(image: image)
        }
    }

    func openMap() {
        guard let user = user else { return}
        mapHelper.openMap(coordinates: user.location.coordinates)
    }
}
